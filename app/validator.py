def validator(validation: dict, request_data):
    data = []

    for key, validation_types in validation.items():
        if type(request_data.get(key, None)) not in validation_types:
            data.append(f'{key} must be {validation_types}, but get {type(request_data.get(key, None))}')

    if data:
        return {'error': 'invalid data', 'data': data}
    else:
        return True
