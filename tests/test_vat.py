import pytest
import random
from app.tax import vat_calculate
from helpers.static_data.vat_calculate import Errors


# For typical VAT assertion. Repeating business logic for easier support in future and dynamic test data generation.
# Imagine what we really doing request for endpoint. Not calling method from nearby file.
def vat_assertion(request_data: dict):
    if request_data.get('vat_included'):
        total_without_vat = request_data.get('money_sum') / (100 + request_data.get('vat_percent')) * 100
        vat = request_data.get('money_sum') - total_without_vat
        total = request_data.get('money_sum')
    else:
        total_without_vat = request_data.get('money_sum')
        vat = request_data.get('money_sum') / 100 * request_data.get('vat_percent')
        total = request_data.get('money_sum') + vat

    return {'total': f'${total:.2f}', 'vat': f'${vat:.2f}',
            'total_without_vat': f'${total_without_vat:.2f}'}


"""
#1 VAT calculate positive test

Steps:
1. Fill field {money} by random value >1 
2. Fill field {vat_percent} by random value 1-100
3. For test when VAT included in sum field {vat_included} must be True
4. Assert with function vat_assertion()

Expected result:
Assert must be passed
"""


@pytest.mark.parametrize("vat_included", [True, False])
def test_vat_calculate(vat_included):
    data = {'money_sum': random.randint(1, 500), 'vat_percent': random.randint(1, 100), 'vat_included': vat_included}
    actual_result = vat_calculate(data)
    expected_result = vat_assertion(data)

    assert expected_result == actual_result


"""
#2 Check boundary values for money and vat_percent
1. money must be >= 0
2. vat_percent must be >= 0
3. vat_included must be bool
"""


@pytest.mark.parametrize("money, vat_percent, vat_included, is_positive_test",
                         [(-1, 5, True, False),
                          (500, -1, False, False),
                          (0, 20, False, True),
                          (50, 0, True, True)]
                         )
def test_tax_with_boundary_values(money, vat_percent, vat_included, is_positive_test):
    data = {'money_sum': money, 'vat_percent': vat_percent, 'vat_included': vat_included}

    if is_positive_test:
        actual_result = vat_calculate(data)
        expected_result = vat_assertion(data)
    else:
        actual_result = vat_calculate(data).get('error')
        expected_result = Errors().invalid_data

    assert expected_result == actual_result

"""
#3 Check invalid values for all fields in vat_calculate

Steps:
1. {money} must be int or float
2. {vat_percent} must be int or float
3. {vat_included} must be bool
"""


def test_vat_calculate_bad_request(types_pairwise):
    data = {'money_sum': types_pairwise[0], 'vat_percent': types_pairwise[1], 'vat_included': types_pairwise[2]}
    result = vat_calculate(data)
    assert result.get('error') == Errors().invalid_data


"""
#3 VAT calculate when sum < 0

Steps:
1. Fill field {money} with negative number
2. Fill field {vat_percent} by value 20
3. Call function

Expected result:
{'error': 'invalid data', 'data': 'Sum or vat percent cannot be < 0'}
"""


def test_vat_calculate_with_negative_money():
    data = {'money_sum': -1, 'vat_percent': 20, 'vat_included': True}
    actual_result = vat_calculate(data)
    expected_result = Errors().invalid_sum_or_vat_percent

    assert expected_result == actual_result


"""
#4 VAT calculate when vat_percent < 0

Steps:
1. Fill field {money} by value 50
2. Fill field {vat_percent} with negative number
3. Call function

Expected result:
{'error': 'invalid data', 'data': 'Sum or vat percent cannot be < 0'}
"""


def test_vat_calculate_with_negative_vat_percent():
    data = {'money_sum': 50, 'vat_percent': -5, 'vat_included': True}
    result = vat_calculate(data)

    assert result == {'error': 'invalid data', 'data': 'Sum or vat percent cannot be < 0'}
