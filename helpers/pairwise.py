from allpairspy import AllPairs


class Pairwise:
    def __init__(self, pairs: list, filter: tuple = None):
        """
        :param pairs: list with lists of pairs which we need to pairwise.
        e.g. [[123, 3221], ['abc', 'cbd']]
        :param filter: which pair we need to skip. format: ('type of validation', 'invalid pair').
        available validations: by type.
        e.g. ('by type', [(int, float), (int, float), (bool,)])
        """
        self.pairs = pairs
        self.filter = filter

    def by_type(self, pairs):
        combination = []
        for i, pair in enumerate(pairs):
            if type(pair) in self.filter[1][i]:
                combination.append(True)
            else:
                combination.append(False)
        if combination == [True for i in self.filter[1]]:
            return False
        else:
            return True

    def generate_pairwise(self):
        for pair in AllPairs(self.pairs, filter_func=eval(f'self.{self.filter[0]}')):
            yield pair

# if __name__ == '__main__':
#     parameters = [
#         ['over9000', 10, 10.5, True, [], {}],
#         ['over9000', 10, 10.5, True, [], {}],
#         ['over9000', 10, 10.5, True, [], {}]
#     ]
#     pairwise = Pairwise(parameters, exclude=('by_type', [(int, float), (int, float), (bool,)]))
#     for pair in pairwise.generate_pairwise():
#        print(pair)